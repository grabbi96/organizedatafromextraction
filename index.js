const data = require("./data.json")
const validator = require("validator")

// organize Data From Extraction function called here
organizeDataFromExtraction()
function organizeDataFromExtraction() {
  data.forEach(element => {
    reArrangeWithFieldLeaf(element)
    reArrangeWithField(element)
  });
}



// re arrange with field leaf
function reArrangeWithFieldLeaf(array) {
  let obj = {}
  array.forEach(ar => {
    // check and add url value
    if (!Object.values(obj).find(it => validator.isURL(it))) {
      if (validator.isURL(ar.content)) {
        obj[[ar.field, ar.leaf].join(".")] = ar.content
      }
    }

    // check and add Uppercase value
    if (!Object.values(obj).find(it => /[A-Z]/.test(it[0]))) {
      if (/[A-Z]/.test(ar.content)) {
        obj[[ar.field, ar.leaf].join(".")] = ar.content
      }
    }

    // check and add price value
    if (!Object.values(obj).find(it => it.startsWith("$"))) {
      if (!Number.isInteger(ar.content[0]) && ar.content.startsWith("$")) {
        obj[[ar.field, ar.leaf].join(".")] = ar.content
      }
    }
  })
}

function reArrangeWithField(array) {
  let obj = {}


  array.forEach(ar => {
    // check and add url value
    if (!Object.values(obj).find(it => validator.isURL(it))) {
      if (validator.isURL(ar.content)) {
        obj[ar.field] = ar.content
      }
    }

    // check and add Uppercase value
    if (!Object.values(obj).find(it => /[A-Z]/.test(it[0]))) {
      if (/[A-Z]/.test(ar.content)) {
        obj[ar.field] = ar.content
      }
    }

    // check and add price value
    if (!Object.values(obj).find(it => it.startsWith("$"))) {
      if (!Number.isInteger(ar.content[0]) && ar.content.startsWith("$")) {
        obj[ar.field] = ar.content
      }
    }

  })
}
